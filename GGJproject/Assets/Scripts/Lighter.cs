﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighter : MonoBehaviour {

	public GameObject LighterObject=null;
	
	// Update is called once per frame
	void Update () {
		float sysHour = System.DateTime.Now.Hour;	
		if ((sysHour > 18) || (sysHour < 6))
			LighterObject.SetActive (true);
		else
			LighterObject.SetActive (false);
	}
}
